//
//  WTClockHand.h
//  What Time
//
//  Created by Ryan Salton on 04/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum {
    ClockHandTypeHour,
    ClockHandTypeMinute,
    ClockHandTypeSecond
} ClockHandType;

@interface WTClockHand : SKSpriteNode

@property (nonatomic, assign) ClockHandType handType;
@property (nonatomic, assign) CGFloat angle;
@property (nonatomic, assign) BOOL animated;

- (instancetype)initWithType:(ClockHandType)handType size:(CGSize)size;

@end
