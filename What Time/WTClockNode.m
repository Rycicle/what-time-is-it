//
//  WTClockNode.m
//  What Time
//
//  Created by Ryan Salton on 04/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "WTClockNode.h"
#import "WTClockHand.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface WTClockNode ()

@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, strong) NSDate *currentDate;
@property (nonatomic, assign) NSInteger currentHour;
@property (nonatomic, assign) NSInteger currentMinute;
@property (nonatomic, assign) NSInteger currentSecond;
@property (nonatomic, strong) WTClockHand *handHour;
@property (nonatomic, strong) WTClockHand *handMinute;
@property (nonatomic, strong) WTClockHand *handSecond;

@property (nonatomic, assign) BOOL animated;
@property (nonatomic, assign) BOOL forceUpdate;

@end


@implementation WTClockNode {
    
}

- (instancetype)initWithColor:(UIColor *)color size:(CGSize)size
{
    self = [super initWithColor:[UIColor clearColor] size: size];
    
    if (self)
    {
        self.hourMod = 0;
        
        self.currentHour = 0;
        self.currentMinute = 0;
        self.currentSecond = 0;
        self.currentDate = [NSDate date];
        
        SKShapeNode *clockBack = [SKShapeNode shapeNodeWithCircleOfRadius:size.width * 0.5];
        clockBack.fillColor = [UIColor whiteColor];
        clockBack.strokeColor = [UIColor blackColor];
        clockBack.lineWidth = 8.0;
        [self addChild:clockBack];
        
        [self createClockIndicators];
        
        self.handSecond.position = CGPointMake(0, 0);
        [self addChild:self.handSecond];
        
        self.handMinute.position = CGPointMake(0, 0);
        [self addChild:self.handMinute];
        
        self.handHour.position = CGPointMake(0, 0);
        [self addChild:self.handHour];
        
        SKShapeNode *clockCenter = [SKShapeNode shapeNodeWithCircleOfRadius:4];
        clockCenter.fillColor = clockBack.strokeColor;
        clockCenter.strokeColor = [UIColor clearColor];
        [self addChild:clockCenter];
    }
    
    return self;
}

- (void)createClockIndicators
{
    NSInteger hourIntervals = 360 / 12;
    NSInteger minuteIntervals = 360 / 60;
    
    for (NSInteger h = 0; h < 12; h++)
    {
        SKSpriteNode *hourHolder = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(5, self.size.width * 0.5)];
        SKShapeNode *hourMarker = [SKShapeNode shapeNodeWithRect:CGRectMake(- (hourHolder.size.width * 0.5), hourHolder.size.height - 10 - 8, hourHolder.size.width, 10)];
        hourMarker.fillColor = [UIColor blackColor];
        hourMarker.strokeColor = [UIColor clearColor];
        
        hourHolder.anchorPoint = CGPointMake(0.5, 0);
        [hourHolder addChild:hourMarker];
        
        [self addChild:hourHolder];
        
        [hourHolder runAction:[SKAction rotateToAngle:DEGREES_TO_RADIANS(hourIntervals * h) duration:0.0]];
    }
    
    for (NSInteger m = 0; m < 60; m++)
    {
        if (m % 5 != 0){
            SKSpriteNode *minuteHolder = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(2, self.size.width * 0.5)];
            SKShapeNode *minuteMarker = [SKShapeNode shapeNodeWithRect:CGRectMake(- (minuteHolder.size.width * 0.5), minuteHolder.size.height - 10 - 8, minuteHolder.size.width, 6)];
            minuteMarker.fillColor = [UIColor blackColor];
            minuteMarker.strokeColor = [UIColor clearColor];
            
            minuteHolder.anchorPoint = CGPointMake(0.5, 0);
            [minuteHolder addChild:minuteMarker];
            
            [self addChild:minuteHolder];
            
            [minuteHolder runAction:[SKAction rotateToAngle:DEGREES_TO_RADIANS(minuteIntervals * m) duration:0.0]];
        }
    }
}

- (NSCalendar *)calendar
{
    if (!_calendar)
    {
        _calendar = [NSCalendar currentCalendar];
    }
    
    return _calendar;
}

- (WTClockHand *)handSecond
{
    if (!_handSecond)
    {
        _handSecond = [[WTClockHand alloc] initWithType:ClockHandTypeSecond size:CGSizeMake(1, (self.size.width * 0.5) - 10)];
    }
    
    return _handSecond;
}

- (WTClockHand *)handMinute
{
    if (!_handMinute)
    {
        _handMinute = [[WTClockHand alloc] initWithType:ClockHandTypeMinute size:CGSizeMake(3, (self.size.width * 0.5) - 10)];
    }
    
    return _handMinute;
}

- (WTClockHand *)handHour
{
    if (!_handHour)
    {
        _handHour = [[WTClockHand alloc] initWithType:ClockHandTypeHour size:CGSizeMake(3, self.size.width * 0.3)];
    }
    
    return _handHour;
}

- (void)setDate:(NSDate *)date
{
    _date = date;
    
    if (date != self.currentDate)
    {
        self.currentDate = date;
        
        if (!self.animated)
        {
            self.animated = YES;
        }
    }
}

- (void)setCurrentDate:(NSDate *)currentDate
{
    _currentDate = currentDate;
    
    if (currentDate)
    {
        NSDateComponents *dateComponents = [self.calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:currentDate];
        
        NSInteger hour = [dateComponents hour];
        NSInteger minute = [dateComponents minute];
        NSInteger second = [dateComponents second];
        
        if (second != self.currentSecond || self.forceUpdate)
        {
            self.forceUpdate = NO;
            
            self.currentSecond = second;
            self.currentMinute = minute;
            self.currentHour = hour + self.hourMod;
            
            NSLog(@"%i:%i:%i\nHOUR MOD: %i", (int)self.currentHour, (int)self.currentMinute, (int)self.currentSecond, (int)self.hourMod);
        }
    }
}

- (void)setCurrentSecond:(NSInteger)currentSecond
{
    _currentSecond = currentSecond;
    
    CGFloat degrees = (360.0 / 60.0) * currentSecond;
    self.handSecond.angle = DEGREES_TO_RADIANS(-degrees);
}

- (void)setCurrentMinute:(NSInteger)currentMinute
{
    _currentMinute = currentMinute;
    
    CGFloat degrees = ((360.0 / 60.0) * currentMinute) + (((360.0 / 60.0) / 60.0) * self.currentSecond);
    self.handMinute.angle = DEGREES_TO_RADIANS(-degrees);
}

- (void)setCurrentHour:(NSInteger)currentHour
{
    _currentHour = currentHour;
    
    CGFloat degrees = ((360 / 12) * currentHour) + (((360.0 / 60.0) / 12.0) * self.currentMinute);
    self.handHour.angle = DEGREES_TO_RADIANS(-degrees);
}

- (void)setHourMod:(NSInteger)hourMod
{
    _hourMod = hourMod;
    
    self.forceUpdate = YES;
    
    [self setCurrentDate:[NSDate date]];
}

- (void)setAnimated:(BOOL)animated
{
    _animated = animated;
    
    self.handHour.animated = animated;
    self.handMinute.animated = animated;
    self.handSecond.animated = animated;
}

@end
