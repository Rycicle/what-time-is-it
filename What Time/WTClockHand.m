//
//  WTClockHand.m
//  What Time
//
//  Created by Ryan Salton on 04/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "WTClockHand.h"

@implementation WTClockHand

- (instancetype)initWithType:(ClockHandType)handType size:(CGSize)size
{
    self = [super initWithColor:[UIColor blackColor] size:size];
    
    if (self)
    {
        self.anchorPoint = CGPointMake(0.5, 0);
        self.handType = handType;
    }
    
    return self;
}

- (void)setAngle:(CGFloat)angle
{
    _angle = angle;
    
    [self runAction:[SKAction rotateToAngle:angle duration:self.animated ? (self.handType == ClockHandTypeHour ? 0.1 : 1.0) : 0.0 shortestUnitArc:YES]];
}

- (void)setHandType:(ClockHandType)handType
{
    _handType = handType;

    if (handType == ClockHandTypeSecond)
    {
        self.color = [UIColor redColor];
        
        NSInteger circleRadius = 3;
        SKShapeNode *circle = [SKShapeNode shapeNodeWithCircleOfRadius:circleRadius];
        circle.fillColor = self.color;
        circle.strokeColor = [UIColor clearColor];
        circle.position = CGPointMake(0, self.size.height - (circleRadius * 3));
        [self addChild:circle];
    }
}

@end
