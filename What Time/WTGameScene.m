//
//  WTGameScene.m
//  What Time
//
//  Created by Ryan Salton on 04/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "WTGameScene.h"
#import "WTClockNode.h"


@interface WTGameScene ()

@property (nonatomic, strong) WTClockNode *clock;

@end


@implementation WTGameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    self.backgroundColor = [UIColor yellowColor];
    
    self.clock = [[WTClockNode alloc] initWithColor:[UIColor redColor] size:CGSizeMake(200, 200)];
    self.clock.position = CGPointMake(self.size.width * 0.5, self.size.height - (self.clock.size.height * 0.5) - 40);
    [self addChild:self.clock];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if (location.x < self.size.width * 0.2)
        {
            self.clock.hourMod--;
        }
        else if (location.x > self.size.width * 0.8)
        {
            self.clock.hourMod++;
        }
        else
        {
            
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    self.clock.date = [NSDate date];
}

@end
