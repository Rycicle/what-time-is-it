//
//  WTClockNode.h
//  What Time
//
//  Created by Ryan Salton on 04/03/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface WTClockNode : SKSpriteNode

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, assign) NSInteger hourMod;

@end
